import { of } from 'rxjs';
import { HeroService } from '../app/hero.service';
import { TEST_HEROES } from './mock-data';

export const mockHeroService = {
  provide: HeroService,
  useValue: {
    getHero() {
      return of(TEST_HEROES[0])
    },
    getHeroes() {
      return of(TEST_HEROES)
    }
  },
};