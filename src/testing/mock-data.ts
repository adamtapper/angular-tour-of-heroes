export const TEST_HEROES = [
    {id: 1, name: 'Hero 1'},
    {id: 2, name: 'Hero 2'},
    {id: 3, name: 'Hero 3'},
    {id: 4, name: 'Hero 4'},
    {id: 5, name: 'Hero 5'}
];