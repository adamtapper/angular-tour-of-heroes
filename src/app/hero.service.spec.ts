import { of } from 'rxjs';
import { HeroService } from './hero.service';
import { MessageService } from './message.service';
import { HEROES } from './mock-heroes';

describe('HeroService', () => {
  let heroService: HeroService;
  let messageService: MessageService;

  beforeEach( () => {
    messageService = new MessageService();
    heroService = new HeroService(messageService);
  });

  test('getHeroes_heroesRetrieved_fetchedHeroesMessageAddedToMessageService', () => {
    heroService.getHeroes();

    expect(messageService.messages).toEqual(['HeroService: fetched heroes']);
  });

  test('getHeroes_heroesRetrieved_heroesListIsMockHeroes', () => {
    const heroes = heroService.getHeroes();

    expect(JSON.stringify(heroes)).toEqual(JSON.stringify(of(HEROES)));
  })
});
