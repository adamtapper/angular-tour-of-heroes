import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { render, screen, fireEvent } from '@testing-library/angular';
import { HeroDetailComponent } from './hero-detail.component';
import { mockHeroService } from '../../testing/mock-services';

describe('Hero Detail', () => {
  const mockGoBack = jest.fn();
  const mockedLocation = {
    provide: Location,
    useValue: {
      back() {
        mockGoBack();
      }
    }
  }

  beforeEach( async () => {
    await render(HeroDetailComponent, {
      imports: [FormsModule],
      componentProviders: [mockHeroService, mockedLocation],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({
                id: '22'
              })
            }
          }
        }
      ]
    });
  });

  describe('Unit Tests', () => {
    test('getHero_heroWithIdAndName_heroDetailsDisplayed', async () => {
      expect(screen.getByText('HERO 1 Details')).toBeTruthy();
      expect(screen.getByText('id:')).toBeTruthy();
      expect(screen.getByText('1')).toBeTruthy();
      expect(screen.getByPlaceholderText('name')).toBeTruthy();
    });

    test('input_flashEnteredAsHeroName_heroRenamedFlash', async () => {
      expect(screen.getByText('HERO 1 Details')).toBeTruthy();
  
      const nameInput = screen.getByPlaceholderText('name');
      fireEvent.input(nameInput, { target: { value: 'Flash' } });
  
      expect(screen.getByText('FLASH Details')).toBeTruthy();
    });

    test('goBack_goBackButtonClicked_locationBackMethodCalled', () => {
      fireEvent.click(screen.getByText('go back'));

      expect(mockGoBack).toHaveBeenCalledTimes(1);
    });
  });
});
