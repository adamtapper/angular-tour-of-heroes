import { RouterModule } from '@angular/router';
import { render, screen } from '@testing-library/angular';
import { HeroesComponent } from './heroes.component';
import { TEST_HEROES } from '../../testing/mock-data';
import { mockHeroService } from '../../testing/mock-services';

describe('Heroes', () => {
  describe('Unit Tests', () => {
    beforeEach( async () => {
      await render(HeroesComponent, {
        imports: [RouterModule],
        componentProviders: [mockHeroService],
        routes: []
      });
    });

    test('getHeroes_testHeroes _heroNamesAndIdsDisplayed', () => {
      TEST_HEROES.forEach(hero => {
        expect(screen.getByText(hero.id)).toBeTruthy();
        expect(screen.getByText(hero.name)).toBeTruthy();
      });
    });

    test('heroLink_heroId_heroDetailsLinkGenerated', () => {
      TEST_HEROES.forEach(hero => {
        const heroLink = screen.getByText(hero.name).closest('a') as Element;

        expect(heroLink.getAttribute('href')).toBe(`/detail/${hero.id}`);
      });
    });
  });

  describe('Component Tests', () => {
    beforeEach( async () => {
      await render(HeroesComponent, {
        imports: [RouterModule],
        routes: []
      });
    });

    test.each([
      [11, 'Dr Nice'],
      [12, 'Narco'],
      [13, 'Bombasto'],
      [14, 'Celeritas'],
      [15, 'Magneta'],
      [16, 'RubberMan'],
      [17, 'Dynama'],
      [18, 'Dr IQ'],
      [19, 'Magma'],
      [20, 'Tornado']
    ])('getHeroes_heroIdIs%i _heroNameDisplayedAs%s', (id, name) => {
      expect(screen.getByText(id)).toBeTruthy();
      expect(screen.getByText(name)).toBeTruthy();
    });
  });
});
