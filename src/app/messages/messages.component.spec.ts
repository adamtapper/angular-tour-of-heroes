import { render, screen, fireEvent } from '@testing-library/angular';
import { MessagesComponent } from './messages.component';
import { MessageService } from '../message.service';

describe('Messages', () => {
  describe('Unit Tests', () => {
    const renderMessageService = async (messageService: MessageService) => {
      await render(MessagesComponent, {
        componentProperties: {
          messageService: messageService
        }
      });
    };

    test('initialRender_messageQueueUndefined_messageComponentHidden', async () => {
      await renderMessageService(new MessageService());

      expect(screen.queryAllByText('Messages').length).toBe(0);
      expect(screen.queryAllByText('clear').length).toBe(0);
    });

    test('initialRender_singleMessageInMessageQueue_messageDisplayed', async () => {
      const fakeMessageService = new MessageService();
      fakeMessageService.messages = ['test message'];
      await renderMessageService(fakeMessageService);

      expect(screen.getByText('Messages')).toBeTruthy();
      expect(screen.getByText('test message')).toBeTruthy();
    });

    test('initialRender_multipleMessagesInMessageQueue_messagesDisplayed', async () => {
      const fakeMessageService = new MessageService();
      fakeMessageService.messages = ['test message 1', 'test message 2'];
      await renderMessageService(fakeMessageService);

      expect(screen.getByText('Messages')).toBeTruthy();
      expect(screen.getByText('test message 1')).toBeTruthy();
      expect(screen.getByText('test message 2')).toBeTruthy();
    });

    test('clear_clearButtonClicked_messageServiceClearMethodCalled', async () => {
      const clearMessageFn = jest.fn();
      const fakeMessageService = new MessageService();
      fakeMessageService.messages = ['test']
      fakeMessageService.clear = clearMessageFn;
      await renderMessageService(fakeMessageService);

      fireEvent.click(screen.getByText('clear'));
  
      expect(clearMessageFn).toHaveBeenCalledTimes(1);
    });
  });
});
