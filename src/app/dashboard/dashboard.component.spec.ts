import { RouterModule } from '@angular/router';
import { render, screen } from '@testing-library/angular';
import { DashboardComponent } from './dashboard.component';
import { mockHeroService } from '../../testing/mock-services';

describe('Dashboard', () => {
  describe('Unit Tests', () => {
    beforeEach( async () => {
      await render(DashboardComponent, {
        imports: [RouterModule],
        componentProviders: [mockHeroService],
        routes: []
      });
    });

    test('getHeroes_dashboardRendered_topFourHeroesDisplayed', () => {
      expect(screen.getByText('Top Heroes')).toBeTruthy();
      expect(screen.getByText('Hero 1')).toBeTruthy();
      expect(screen.getByText('Hero 2')).toBeTruthy();
      expect(screen.getByText('Hero 3')).toBeTruthy();
      expect(screen.getByText('Hero 4')).toBeTruthy();
      expect(screen.queryByText('Hero 5')).not.toBeTruthy();
    });

    test('heroTile_heroId_heroDetailsLinkGenerated', () => {
      const heroTile = screen.getAllByTestId('heroLink').find(
        el => el.textContent === 'Hero 1') as Element;

      expect(heroTile.getAttribute('href')).toBe('/detail/1');
    });
  });

  describe('Component Tests', () => {
    test('getHeroes_dashboardRendered_topFourHeroesDisplayed', async () => {
      await render(DashboardComponent, {
        imports: [RouterModule],
        routes: []
      });

      expect(screen.getByText('Top Heroes')).toBeTruthy();
      expect(screen.getByText('Dr Nice')).toBeTruthy();
      expect(screen.getByText('Narco')).toBeTruthy();
      expect(screen.getByText('Bombasto')).toBeTruthy();
      expect(screen.getByText('Celeritas')).toBeTruthy();
    });
  });
});
