import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { render, screen, fireEvent, RenderResult } from '@testing-library/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { mockHeroService } from '../testing/mock-services';

describe('Unit Tests', () => {
    beforeEach(async () => {
        await render(AppComponent, {
            declarations: [MessagesComponent],
            imports: [RouterModule],
            componentProviders: [mockHeroService],
            routes: []
        });
    });

    test('initialRender_appTitleDisplayed', async () => {
        expect(screen.getByText('Tour of Heroes')).toBeTruthy();
    });

    test('initialRender_dashboardLinkDisplayed', () => {
        expect(screen.getByText('Dashboard').tagName).toBe('A');
    });

    test('initialRender_heroesLinkDisplayed', () => {
        expect(screen.getByText('Heroes').tagName).toBe('A');
    });
});

describe('Component Tests', () => {
    let app: RenderResult<AppComponent>;
    beforeEach( async () => {
        app = await render(AppComponent, {
            declarations: [DashboardComponent, HeroesComponent, HeroDetailComponent, MessagesComponent],
            imports: [FormsModule, AppRoutingModule],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        });
    });

    describe('Dashboard', () => {
        test('routing_dashboardLinkClicked_dashboardDisplayed', async () => {
            expect(screen.queryByText('Top Heroes')).toBeFalsy();

            await app.navigate(screen.getByText('Dashboard'));

            expect(screen.getByText('Top Heroes')).toBeTruthy();
        });

        test('heroTile_clickHeroTile_heroDetailsDisplayed', async () => {
            await app.navigate(screen.getByText('Dashboard'));

            await app.navigate(screen.getAllByTestId('heroLink').find(
                el => el.textContent === 'Narco') as Element);

            expect(screen.getByText('NARCO Details')).toBeTruthy();
            expect(screen.getByText('12')).toBeTruthy();
            expect(screen.getByPlaceholderText('name')).toBeTruthy();
        });
    });

    describe('Heroes', () => {
        test('routing_heroesLinkClicked_heroesDisplayed', async () => {
            expect(screen.queryByText('My Heroes')).toBeFalsy();

            await app.navigate(screen.getByText('Heroes'));

            expect(screen.getByText('My Heroes')).toBeTruthy();
        });

        test('heroLink_clickHeroLink_heroDetailsDisplayed', async () => {
            await app.navigate(screen.getByText('Heroes'));

            await app.navigate(screen.getByText('Bombasto'));
        
            expect(screen.getByText('BOMBASTO Details')).toBeTruthy();
            expect(screen.getByText('id:')).toBeTruthy();
            expect(screen.getAllByText('13')).toBeTruthy();
            expect(screen.getByPlaceholderText('name')).toBeTruthy();
        });
    });

    describe('Messages', () => {
        test('add_multipleButtonsClicked_fetchedHeroMessageAddedToQueue', async () => {
            await app.navigate(screen.getByText('Dashboard'));

            expect(screen.getByText('Messages')).toBeTruthy();
            expect(screen.getByText('HeroService: fetched heroes')).toBeTruthy();

            await app.navigate(screen.getByText('Heroes'));

            expect(screen.getByText('Messages')).toBeTruthy();
            expect(screen.getAllByText('HeroService: fetched heroes').length).toBe(2);
        });

        test('add_heroClicked_fetchedHeroDetailsMessageAddedToQueue', async () => {
            await app.navigate(screen.getByText('Dashboard'));
            await app.navigate(screen.getAllByTestId('heroLink').find(
                el => el.textContent === 'Narco') as Element);

            expect(screen.getByText('Messages')).toBeTruthy();
            expect(screen.getByText('HeroService: fetched hero id=12')).toBeTruthy();

            await app.navigate(screen.getByText('Heroes'));
            await app.navigate(screen.getByText('Bombasto'));

            expect(screen.getByText('Messages')).toBeTruthy();
            expect(screen.getByText('HeroService: fetched hero id=12')).toBeTruthy();
            expect(screen.getByText('HeroService: fetched hero id=13')).toBeTruthy();
        });

        test('clear_singleMessageInQueue_messageComponentHidden', async () => {
            await app.navigate(screen.getByText('Dashboard'));
    
            fireEvent.click(screen.getByText('clear'));

            expect(screen.queryAllByText('Messages').length).toBe(0);
            expect(screen.queryAllByText('clear').length).toBe(0);
            expect(screen.queryAllByText('HeroService: fetched heroes').length).toBe(0);
        });

        test('clear_messageAddedAfterClearingQueue_messageComponentDisplayed', async () => {
            await app.navigate(screen.getByText('Heroes'));

            fireEvent.click(screen.getByText('clear'));

            await app.navigate(screen.getByText('Dynama'));

            expect(screen.getByText('Messages')).toBeTruthy();
            expect(screen.getByText('clear')).toBeTruthy();
            expect(screen.getByText('HeroService: fetched hero id=17')).toBeTruthy();
        });
    });
});