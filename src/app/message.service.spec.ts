import { MessageService } from './message.service';

describe('MessageService', () => {
  let service: MessageService

  beforeEach( () => {
    service = new MessageService();
  });

  test('add_singleTestMessage_messageAddedToMessagesArray', () => {
    service.add('test message');

    expect(service.messages).toEqual(['test message']);
  });

  test('add_multipleTestMessages_messagesAddedToMessagesArrayInOrder', () => {
    service.add('test message 1');
    service.add('test message 2');

    expect(service.messages).toEqual(['test message 1', 'test message 2']);
  });

  test('clear_singleMessageInQueue_messageErased', () => {
    service.messages = ['test message'];

    service.clear();

    expect(service.messages).toEqual([]);
  });

  test('clear_multipleMessagesInQueue_messagesErased', () => {
    service.messages = ['test message 1', 'test message 2'];

    service.clear();

    expect(service.messages).toEqual([]);
  });
});
